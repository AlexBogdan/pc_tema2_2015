#include <stdio.h>
#include <stdlib.h>

void reset ();
void fill (int i , int ji , int flag);
void decodificare (int x);
void baza10_2 (int x);
int  baza2_10 (int x , int y);

int b , n , m;
char temp[32]; // Aici se fac transformarile din 10 in 2
int aux = 0;//verifica cand s-a atins limita de biti 'b' in functia decodificare
int cnt; // Contor global pentru vectorul decodificare

char decodificat [5485760];// Alocam 10 Mb memorie pt vectorul de biti decodificati
int teren[402][402]; // Matricea pentru task 3
int task3 [160005];

void reset () //Reseteaza temp[] la 0
{
	int i;
	for (i=0 ; i < 32 ; i++)
		{temp[i] = 0;}
}

void fill (int i , int ji , int flag)
{
	teren[i][ji] = flag;
	if (i-1  >= 0)
		if (teren[i-1][ji] == 1) 
			fill (i-1 , ji , flag);
	if (ji+1 <  n)
		if (teren[i][ji+1] == 1)
			fill (i , ji+1 , flag);
	if (i+1  <  m)
		if (teren[i+1][ji] == 1)
			fill (i+1 , ji , flag);
	if (ji-1 >= 0)
		if (teren[i][ji-1] == 1) 
			fill (i , ji-1 , flag);
}

void baza10_2 (int x)
{
	int i;
	
	for (i=0 ; x ; i++)
	{
		temp[i] = x%2;
		x /= 2;
	}
}

int baza2_10 (int x , int y)
{
	int putere = 1 , nr = 0 , i;
	i = y - x;
	for ( ; i>0 ; i--) putere *= 2;
	for ( ; x<=y ; x++)
	{
		nr += decodificat[x]*putere;
		putere /= 2;
	}

	return nr;
}

void decodificare ( int x )
{
	int ji;

	if (x == 0)
	{
		if (aux == 0) aux = b;

		decodificat [cnt] = 0;
		aux--; cnt++;
	}
	else
	{
		baza10_2 (x);

		for (ji=31 ; ji>=0 && temp[ji] == 0 ; ji--);

		if (aux == 0) aux = b;
		while (aux && ji >= 0 )
		{
			decodificat [cnt] = temp[ji];
			aux--; cnt++; ji--;
		}
	}

	reset ();
}

int main ()
{
	int x , y , i , ji , nr , linie , coloana;

	//-=-=-=-=-=-=-=-=-=--=   Task 1   -=-=-=-=-=-=-=-=-=-=-=-=-=-


	scanf ("%d%d%d" , &b , &m , &n);

	scanf ("%d" , &x);
	while (x!=-1)
	{
		decodificare (x);
		scanf ("%d" , &x);
	}

	cnt --;
	for (i=0 ; i <= cnt ; i++)
		printf ("%d " , decodificat[i] );
	printf("\n");

//-=-=-=-=-=-=-=-=-=--=   Task 2   -=-=-=-=-=-=-=-=-=-=-=-=-=-

	scanf ("%d%d" , &x , &y);

	while ( x != -1 || y != -1)
	{
		if (x < 0) x = 0;
		if (y > cnt) y = cnt;
		if (y - x > 30)
			y = x + 30;

		if ( x < cnt && x <= y && y >= 0)
		{
			
			nr = baza2_10 (x , y);
	
			if ( nr >= m*n ) nr = nr % (m*n);

			linie = nr / n;
			coloana = nr % n;
			teren[linie][coloana] = 1;

			printf ("%d " , nr);
		}
		scanf ("%d%d" , &x , &y);
	}

	int flag = -1 , maxi = -1;

	for (i=0 ; i < m ; i++)
		for (ji=0 ; ji < n ; ji++)
			if (teren[i][ji] == 1)
			{
				fill (i , ji , flag);
				flag--;
			}
	flag ++;

	for (i=0 ; i < m ; i++)
		for (ji=0 ; ji < n ; ji++)
			task3 [ abs(teren[i][ji]) ] ++;

	for (i = 1 ; i <= abs(flag) ; i++)
		if (maxi < task3[i]) maxi = task3[i];

	int flags [abs(flag) + 5];

	ji=1;
	for (i = 1 ; i <= abs(flag) ; i++)
		if (maxi == task3[i])
		{
			flags[ji] = i;
			ji++;
		}
	ji--;
	x = ji;

	for (nr =  1 ; nr <= x ; nr++)
		for (i=0 ; i < m ; i++)
			for (ji=0 ; ji < n ; ji++)
				if (teren[i][ji] == -(flags[nr]))
				{
					printf("\n%d %d %d", i , ji , maxi);
					i = m; ji = n;
				}

	printf("\n");
	return 0;
}
