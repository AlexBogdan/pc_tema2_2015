Taskul 1 foloseste vectorul de 10 Mb de memorie , functia de baza_10_2 si functia de decodificare. Astfel ca la taskul 1 se salveaza in vectorul tmp bitii numerelor introduse in temp , dupa se copiaza in vectorul mare pana cand se atinge limta impusa 'b' , dupa care se reseteaza contorii , etc. . Astfel se va forma sirul de biti pentru taskul 1.  Am ales foarte multa memorie , nestiind cate numere de intrare sunt.

Taskul 2 decodifica conform cerintei si marcheaza in matricea teren bazele gasite.

Taskul 3 foloseste functia fill pentru a marca toate reuniunile posibile cu flaguri (indiferent daca reuniunea este formata doar dintr-o baza). Dupa aflu reuniunile cu numarul maxim de steaguri si afisez conform cerintei. Functia fill este una recursiva.